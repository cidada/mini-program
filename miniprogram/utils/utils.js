class Utils {
  formatDate(date, format) {
    //date: 日期, string | Date
    //format: 日期格式, string

    //例如：yyyy-MM-dd hh:mm:ss ==> 2023-04-03 10:02:10
    //例如：yy-MM-dd hh:mm:ss ==> 23-04-03 10:02:10
    //例如：yyyy@MM-dd hh:mm:ss ==> 2023@04-03 10:02:10
    //例如：yyyy@MM-dd mm:ss ==> 2023@04-03 02:10
    //例如：yyyy@MM/d mm:ss ==> 2023@04/3 02:10
    //例如：yyyy@M/d mm:ss ==> 2023@4/3 02:10
    //例如：yyyy#M/d ==> 2023#4/3
    //例如：hh*mm:ss ==> 10*02:10
    //例如：MM/d mm:ss ==> 04/3 02:10
    //....

    if (Object.prototype.toString.call(date) !== '[object Date]') {
      date = new Date(date);
    }

    if (/(y+)/.test(format)) {

      //获取年份
      let year = date.getFullYear().toString();

      //获取组匹配的内容
      // console.log('RegExp.$1 ==> ', RegExp.$1);

      format = format.replace(RegExp.$1, year.slice(year.length - RegExp.$1.length));


    }


    let o = {
      M: date.getMonth() + 1,
      d: date.getDate(),
      h: date.getHours(),
      m: date.getMinutes(),
      s: date.getSeconds()
    };


    for (let key in o) {
      //动态创建正则表达式
      let reg = new RegExp(`(${key}+)`);

      if (reg.test(format)) {
        //获取匹配组的内容
        // console.log('RegExp.$1 ==> ', RegExp.$1);

        //不足10补零
        format = format.replace(RegExp.$1, o[key] >= 10 || RegExp.$1.length === 1 ? o[key] : `0${o[key]}`);

        // console.log('format ==> ', format);

      }
    }

    return format;

  }
}

//commonJS规范
//导出
module.exports = new Utils();