const utils = require('../../utils/utils.js');
Page({
  data: {
    userId: '',
    Info:'',
    favorites: [],
    isFavorites: false,
    petDetail: [],
    userInfo: [],
    genderImg: '',
    received: [

    ],
    request: [],
  },
  getLoginUser() {
    let token = wx.getStorageSync('token');
    // console.log('token ==> ', token);
    if (!token) {
      return;
    }
    wx.cloud.callFunction({
      name: 'get-user-login',
      data: {
        token
      }
    }).then(res => {
      console.log(res);
      let tokenData = res.result.data
      for (let i = 0; i < tokenData.length; i++) {
        if (tokenData[i].token == token) {
          this.setData({
            ['userId']: tokenData[i].userId
          })
        }
      }
      this.getUserId()
      this.getLUser()
      // console.log('this.data.userId ==> ', this.data.userId);
    }).catch(err => {
      console.log(err);
    })
  },
  getToken() {
    if (wx.getStorageSync('token')) {} else {
      wx.showToast({
        title: '账号未登录，即将跳到登录页',
        icon: 'none'
      })
      setTimeout(() => {
        this.goLogin()
      }, 2000)
    }
  },
  Favorites(e) {
    this.setData({
      isFavorites: !this.data.isFavorites
    })
    let id = e.currentTarget.dataset.item._id
    if (this.data.favorites.findIndex((value) => value._id == id) == -1) {
      this.data.favorites.push(e.currentTarget.dataset.item)
    } else if (this.data.favorites.findIndex((value) => value._id == id) != -1) {
      this.data.favorites.splice(this.data.favorites.findIndex((value) => value._id == id), 1)
    }
    wx.cloud.callFunction({
      name: 'save-favorites',
      data: {
        favorites: this.data.favorites,
        userId: this.data.userId
      },
    }).then(res => {
      console.log('res ==> ', res);
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
  getPetDetail(id) {
    wx.cloud.callFunction({
      name: 'get-pet-detail',
      data: {
        id: id,
      }
    }).then(res => {
      // console.log('res ==> ', res);
      res.result.data.forEach(item => {
        item.date = utils.formatDate(item.date, 'yyyy-MM-dd hh:mm:ss');
      })
      if (res.result.data[0].petAdopt.gender == "女生") {
        this.setData({
          genderImg: 'cloud://cloud1-5g0q9ea87e48edbf.636c-cloud1-5g0q9ea87e48edbf-1318245778/img/女.png'
        })
      } else if (res.result.data[0].petAdopt.gender == "男生") {
        this.setData({
          genderImg: 'cloud://cloud1-5g0q9ea87e48edbf.636c-cloud1-5g0q9ea87e48edbf-1318245778/img/男.png'
        })
      }
      this.setData({
        petDetail: res.result.data[0]
      })
      this.getUser(res.result.data[0].userId)
      console.log('petDetail', this.data.petDetail);
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
  getUserId() {
    wx.cloud.callFunction({
      name: 'get-user',
      data: {
        userId: this.data.userId
      },
    }).then(res => {
      this.setData({
        favorites: res.result.data[0].favorites
      })
      this.getfavorites()
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
  getLUser() {
    wx.cloud.callFunction({
      name: 'get-user',
      data: {
        userId: this.data.userId
      }
    }).then(res => {
      // console.log('res66 ==>', res);
      this.setData({
        Info: res.result.data[0]
      })
    }).catch(err => {
      console.log('err66 ==> ', err);
    })
  },
  getUser(id) {
    wx.cloud.callFunction({
      name: 'get-user',
      data: {
        userId: id
      }
    }).then(res => {
      // console.log('res66 ==>', res);
      this.setData({
        userInfo: res.result.data[0]
      })
      console.log(this.data.userInfo);
    }).catch(err => {
      console.log('err66 ==> ', err);
    })
  },
  goLogin() {
    wx.navigateTo({
      url: '../login/login',
    })
  },
  getfavorites() {
    console.log(this.data.favorites.findIndex((value) => value._id == this.data.petDetail._id), '666');
    if (this.data.favorites.findIndex((value) => value._id == this.data.petDetail._id) == -1) {
      this.setData({
        isFavorites: false
      })
    } else {
      this.setData({
        isFavorites: true
      })
    }
  },
  onLoad(options) {
    this.getPetDetail(options.id)
  },
  onShow() {
    this.getToken()
    this.getLoginUser()
  },
  requestPet(e) {
    let id = e.currentTarget.dataset.item._id
    let item = e.currentTarget.dataset.item
    item.Info = this.data.Info
    item.isSure = false
    if (this.data.received.findIndex((value) => value._id == id) == -1) {
      this.data.received.push(item)
    } else {
      return
    }
    wx.cloud.callFunction({
      name: 'save-received',
      data: {
        received: this.data.received,
        userId: this.data.userInfo.userId
      },
    }).then(res => {
      console.log('res ==> ', res);
    }).catch(err => {
      console.log('err ==> ', err);
    })
    wx.cloud.callFunction({
      name: 'remove-pet',
      data: {
        id:id
      },
    }).then(res => {
      console.log('res ==> ', res);
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
  requestPet1(e) {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
    if (this.data.request.findIndex((value) => value._id == id) == -1) {
      this.data.request.push(e.currentTarget.dataset.item)
    } else {
      return
    }
    wx.cloud.callFunction({
      name: 'save-request',
      data: {
        request: this.data.request,
        userId: this.data.userId,
      },
    }).then(res => {
      wx.hideLoading();
      console.log('res ==> ', res);
      wx.showToast({
        title: res.result.msg,
        icon: 'none',
        duration: 2000
      })
    }).catch(err => {
      wx.hideLoading();
      console.log('err ==> ', err);
    })
  },
  requestPetAll(e){
    this.requestPet(e)
    this.requestPet1(e)
  }
})