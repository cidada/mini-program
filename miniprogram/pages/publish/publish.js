Page({
  data: {
    publishList: []
  },
  getPublishList() {
    wx.cloud.callFunction({
      name: 'get-publish-list',
    }).then(res => {
      // console.log('publishList ==> ', res);
      this.publishList = res.result.data
      this.setData({
        publishList: res.result.data
      })
      // console.log('this.publishList ==> ', this.publishList);
    }).catch(err => {
      console.log('publishList ==> ', err);
    })
  },
  goPublishPet(){
    wx.navigateTo({
      url: '../publishPet/publishPet',
    })
  },
  goAdoptKnows(){
    wx.navigateTo({
      url: '../adoptKnows/adoptKnows',
    })
  },
  onShow() {
    this.getPublishList();
  },
})