Page({
  data: {
    petList:'',
    userId:'',
  },
  getLoginUser() {
    let token = wx.getStorageSync('token');
    // console.log('token ==> ', token);
    if (!token) {
      return;
    }
    wx.cloud.callFunction({
      name: 'get-user-login',
      data: {
        token
      }
    }).then(res => {
      let tokenData = res.result.data
      for (let i = 0; i < tokenData.length; i++) {
        if (tokenData[i].token == token) {
          this.setData({
            userId: tokenData[i].userId
          })
        }
      }
      this.getPet()
    }).catch(err => {
      console.log(err);
    })
  },
  getPet() {
    wx.cloud.callFunction({
      name: 'get-my-pet',
      data: {
        userId: this.data.userId,
      }
    }).then(res => {
      console.log('res ==> ', res);
      this.setData({
        petList: res.result.data
      })
      console.log('petList', this.data.petList);
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
  onLoad(options) {

  },
  onShow() {
    this.getLoginUser()
  },
})