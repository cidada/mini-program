Page({
  data: {
    myList: [],
    userInfo: '',
    userId: '',
    petList:'',
  },
  //获取我的页面的功能栏
  getMyList() {
    wx.cloud.callFunction({
      name: 'get-my-list',
    }).then(res => {
      // console.log('mylist ==> ', res);
      // this.data.myList = res.result.data
      this.setData({
        myList: res.result.data
      })
    }).catch(err => {
      console.log('mylist ==> ', err);
    })
  },
  goEdit() {
    wx.navigateTo({
      url: '../editUserInfo/editUserInfo',
    })
  },
  getLoginUser() {
    let token = wx.getStorageSync('token');
    // console.log('token ==> ', token);
    if (!token) {
      return;
    }
    wx.cloud.callFunction({
      name: 'get-user-login',
      data: {
        token
      }
    }).then(res => {
      let tokenData = res.result.data
      for (let i = 0; i < tokenData.length; i++) {
        if (tokenData[i].token == token) {
          this.setData({
            userId: tokenData[i].userId
          })
        }
      }
      this.getUser()
      this.getPet()
    }).catch(err => {
      console.log(err);
    })
  },
  getUser() {
    wx.cloud.callFunction({
      name: 'get-user',
      data: {
        userId: this.data.userId
      }
    }).then(res => {
      // console.log('res66 ==>', res);
      this.setData({
        userInfo: res.result.data[0]
      })
    }).catch(err => {
      console.log('err66 ==> ', err);
    })
  },
  goHome() {
    wx.switchTab({
      url: '../home/home',
    })
  },
  removeToken() {
    wx.removeStorage({
      key: 'token',
    })
    wx.showToast({
      title: '退出成功',
    })
    setTimeout(() => {
      this.goHome()
    }, 2000)
  },
  onShow() {
    this.getMyList();
    this.getLoginUser();
    this.getToken();
  },
  onLoad() {
    this.getToken();
  },
  goLogin() {
    wx.navigateTo({
      url: '../login/login',
    })
  },
  //获取本地存储token
  getToken() {
    if (wx.getStorageSync('token')) {
    } else {
      wx.showToast({
        title: '账号未登录，即将跳到登录页',
        icon: 'none'
      })
      setTimeout(() => {
        this.goLogin()
      }, 2000)
    }
  },
  goFavorites(){
    wx.navigateTo({
      url: '../favorites/favorites',
    })
  },
  goMyPublish(){
    wx.navigateTo({
      url: '../myPublish/myPublish',
    })
  },
  getPet() {
    wx.cloud.callFunction({
      name: 'get-my-pet',
      data: {
        userId: this.data.userId,
      }
    }).then(res => {
      console.log('res ==> ', res);
      this.setData({
        petList: res.result.data
      })
      // console.log('petList', this.data.petList);
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
  goRequest(){
    wx.navigateTo({
      url: '../request/request',
    })
  },
  goReceived(){
    wx.navigateTo({
      url: '../received/received',
    })
  }
})