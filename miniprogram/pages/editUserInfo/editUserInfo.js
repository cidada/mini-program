Page({
  data: {
    userId: '',
    region: ['', '', ''],
    date: '',
    phone: '',
    userImg: '',
    userInfo: {
      nickname: '',
      birthday: '',
      address: '',
      synopsis: '',
    }

  },
  //修改userInfo的数据
  updateUserInfo(e) {
    let key = e.currentTarget.dataset.key;
    this.data.userInfo[key] = e.detail.value;
  },
  bindDateChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      date: e.detail.value,
      ['userInfo.birthday']: e.detail.value
    })
  },
  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value,
      ['userInfo.address']: e.detail.value
    })
    // console.log(this.data.userInfo, '6666');
  },
  save() {
    wx.cloud.callFunction({
      name: 'save-userInfo',
      data: {
        ...this.data.userInfo,
        userId: this.data.userId
      },
    }).then(res => {
      // console.log('res ==> ', res);
      wx.showToast({
        title: '保存成功',
        icon:'none'
      })
      setTimeout(()=>{
        wx.switchTab({
          url: '../my/my',
        },2000)
      })
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
  getLoginUser() {
    let token = wx.getStorageSync('token');
    // console.log('token ==> ', token);
    if (!token) {
      return;
    }
    wx.cloud.callFunction({
      name: 'get-user-login',
      data: {
        token
      }
    }).then(res => {
      console.log(res);
      let tokenData = res.result.data
      for (let i = 0; i < tokenData.length; i++) {
        if (tokenData[i].token == token) {
          this.setData({
            ['userId']: tokenData[i].userId
          })
        }
      }
      this.getUser(this.data.userId)
      // console.log('this.data.userId ==> ', this.data.userId);
    }).catch(err => {
      console.log(err);
    })


  },
  getUser(userId) {
    wx.cloud.callFunction({
      name: 'get-user',
      data: {
        userId: userId
      }
    }).then(res => {
      // console.log('res66 ==>', res);
      this.setData({
        ['phone']: res.result.data[0].phone,
        ['date']: res.result.data[0].birthday,
        ['userInfo.nickname']: res.result.data[0].nickname,
        ['region']: res.result.data[0].address,
        ['userInfo.synopsis']: res.result.data[0].synopsis,
        ['userInfo.birthday']: res.result.data[0].birthday,
        ['userInfo.address']: res.result.data[0].address,
        userImg:res.result.data[0].userImg
      })
    }).catch(err => {
      console.log('err66 ==> ', err);
    })
  },
  //上传用户头像
  uploadUserImg() {
    console.log('上传用户头像');
    wx.chooseMedia({
      //上传文件数量
      count: 1,
      //上传文件类型
      mediaType: ['image'],
      //选择图片来源(相册选择和拍摄)
      sourceType: ['album', 'camera'],
      //视频最大拍摄时间
      maxDuration: 30,
      //使用后置摄像头
      camera: 'back',
      //成功选择文件后执行
      success: res => {
        console.log('res ==> ', res);
        //获取本地临时路径
        let tempFilePath = res.tempFiles[0].tempFilePath;
        console.log('tempFilePath ==> ', tempFilePath);
        //生成文件名
        let filename = tempFilePath.split(/\//).pop();
        console.log('filename ==> ', filename);
        wx.cloud.uploadFile({
          cloudPath: `userImg/${filename}`, //云存储的文件路径
          filePath: tempFilePath, //本地临时路径
        }).then(res => {
          // get resource ID
          console.log(res.fileID);
          //修改用户头像的路径
          wx.cloud.callFunction({
            name: 'updata-user-img',
            data: {
              userImg: res.fileID,
              userId: this.data.userId
            }
          }).then(data => {
            console.log('data ==> ', data);
            if (data.result.stats.updated === 1) {
              this.data.userImg = res.fileID;
              this.setData({
                userImg: res.fileID
              })
            }
          }).catch(err => {
            console.log('err ==> ', err);
          })
        }).catch(error => {
          // handle error
          console.log('error ==> ', error);
        })
      },
      //失败选择文件后执行
      fail: err => {
        console.log('err ==> ', err);
      }
    })
  },
  onLoad(options) {

  },
  onShow() {
    this.getLoginUser()
  },
})