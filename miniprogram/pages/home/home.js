Page({
  data: {
    userId: '',
    petList: [],
  },
  gotoAdopt() {
    wx.switchTab({
      url: '../adopt/adopt',
    })
  },
  getLoginUser() {
    let token = wx.getStorageSync('token');
    // console.log('token ==> ', token);
    if (!token) {
      return;
    }
    wx.cloud.callFunction({
      name: 'get-user-login',
      data: {
        token
      }
    }).then(res => {
      let tokenData = res.result.data
      for (let i = 0; i < tokenData.length; i++) {
        if (tokenData[i].token == token) {
          this.setData({
            userId: tokenData[i].userId
          })
        }
      }
      this.getPet()
    }).catch(err => {
      console.log(err);
    })
  },
  goDetail(e) {
    // console.log(e);
    wx.navigateTo({
      url: '../detail/detail?id=' + e.currentTarget.dataset.id + '',
    })
  },
  getPet() {
    let token = wx.getStorageSync('token');
    wx.cloud.callFunction({
      name: 'get-pet',
      data: {
        userId: this.data.userId,
        token
      }
    }).then(res => {
      console.log('res ==> ', res);
      this.setData({
        petList: res.result.data
      })
      console.log('petList', this.data.petList);
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
  onLoad(options) {

  },
  onShow() {
    let token = wx.getStorageSync('token');
    this.getLoginUser()
    if(token){

    }else{
      this.getPet()
    }
  },
})