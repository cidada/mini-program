// pages/message/message.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    messageList: []
  },
  getMessageList() {
    wx.cloud.callFunction({
      name: 'get-message-list',
    }).then(res => {
      // console.log(res);
      this.messageList = res.result.data
      this.setData({
        messageList: res.result.data
      })
    }).catch(err => {
      console.log(err);
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getMessageList();
  },
})