Page({

  data: {
    userId: '',
    userInfo: '',
  },
  getLoginUser() {
    let token = wx.getStorageSync('token');
    // console.log('token ==> ', token);
    if (!token) {
      return;
    }
    wx.cloud.callFunction({
      name: 'get-user-login',
      data: {
        token
      }
    }).then(res => {
      let tokenData = res.result.data
      for (let i = 0; i < tokenData.length; i++) {
        if (tokenData[i].token == token) {
          this.setData({
            userId: tokenData[i].userId
          })
        }
      }
      this.getUser()
    }).catch(err => {
      console.log(err);
    })
  },
  getUser() {
    wx.cloud.callFunction({
      name: 'get-user',
      data: {
        userId: this.data.userId
      }
    }).then(res => {
      // console.log('res66 ==>', res);
      this.setData({
        userInfo: res.result.data[0]
      })
      console.log(this.data.userInfo);
    }).catch(err => {
      console.log('err66 ==> ', err);
    })
  },
  goDetail(e) {
    wx.navigateTo({
      url: '../detail/detail?id=' + e.currentTarget.dataset.id + '',
    })
  },
  onLoad(options) {

  },
  onShow() {
    this.getLoginUser()
  },
})