const citySelector = requirePlugin('citySelector');
const utils = require('../../utils/utils.js');
Page({
  data: {
    userId: '',
    petList: [],
    isSearch: true,
    location: '全国',
    isAdoptLabel: false,
    search: '',
  },
  observers: {
    'location': function () {
      this.getSearchPet()
    },
  },
  setIsSearch() {
    this.isSearch = !this.isSearch
    this.setData({
      isSearch: this.isSearch
    })
  },
  getdz() {
    const key = 'SG7BZ-BMV67-FMNXW-HEATY-KGAWQ-BUFTN'; // 使用在腾讯位置服务申请的key
    const referer = 'wechat'; // 调用插件的app的名称
    wx.navigateTo({
      url: `plugin://citySelector/index?key=${key}&referer=${referer}`,
    })
  },
  onShow() {
    const selectedCity = citySelector.getCity();
    // 选择城市后返回城市信息对象，若未选择返回null
    // console.log(selectedCity);
    if (selectedCity) {
      this.setData({
        location: selectedCity.name
      })
      this.getSearchPet()
    }
  },
  onLoad() {
    let token = wx.getStorageSync('token');
    this.getLoginUser()
    if (token) {

    } else {
      this.getPet()
    }
  },
  onUnload() {
    // 页面卸载时清空插件数据，防止再次进入页面，getCity返回的是上次的结果
    citySelector.clearCity();

  },
  getInputValue(e) {
    // console.log(e.detail.value);
    this.setData({
      search: e.detail.value
    })
    // if (this.data.search != '') {
      this.getSearchPet()
    // } else {
    //   this.getPet()
    // }

    // console.log(this.data.search);
  },
  getPet() {
    let token = wx.getStorageSync('token');
    wx.cloud.callFunction({
      name: 'get-pet',
      data: {
        userId: this.data.userId,
        token
      }
    }).then(res => {
      console.log('res ==> ', res);
      res.result.data.forEach(item => {
        item.date = utils.formatDate(item.date, 'yyyy-MM-dd hh:mm:ss');
      })
      this.setData({
        petList: res.result.data
      })
      console.log('petList', this.data.petList);
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
  goDetail(e) {
    // console.log(e);
    wx.navigateTo({
      url: '../detail/detail?id=' + e.currentTarget.dataset.id + '',
    })
  },
  getLoginUser() {
    let token = wx.getStorageSync('token');
    // console.log('token ==> ', token);
    if (!token) {
      return;
    }
    wx.cloud.callFunction({
      name: 'get-user-login',
      data: {
        token
      }
    }).then(res => {
      let tokenData = res.result.data
      for (let i = 0; i < tokenData.length; i++) {
        if (tokenData[i].token == token) {
          this.setData({
            userId: tokenData[i].userId
          })
        }
      }
      this.getPet()
    }).catch(err => {
      console.log(err);
    })
  },
  getSearchPet() {
    wx.cloud.callFunction({
      name: 'get-search-pet',
      data: {
        userId: this.data.userId,
        location: this.data.location,
        search: this.data.search,
      }
    }).then(res => {
      console.log('res66 ==> ', res);
      res.result.data.forEach(item => {
        item.date = utils.formatDate(item.date, 'yyyy-MM-dd hh:mm:ss');
      })
      this.setData({
        petList: res.result.data
      })
      // console.log('petList', this.data.petList);
    }).catch(err => {
      console.log('err ==> ', err);
    })
  },
})
// Component({

// })