Page({
  data: {
    userId: '',
    userInfo: '',
  },
  getLoginUser() {
    let token = wx.getStorageSync('token');
    // console.log('token ==> ', token);
    if (!token) {
      return;
    }
    wx.cloud.callFunction({
      name: 'get-user-login',
      data: {
        token
      }
    }).then(res => {
      let tokenData = res.result.data
      for (let i = 0; i < tokenData.length; i++) {
        if (tokenData[i].token == token) {
          this.setData({
            userId: tokenData[i].userId
          })
        }
      }
      this.getUser()
    }).catch(err => {
      console.log(err);
    })
  },
  getUser() {
    wx.cloud.callFunction({
      name: 'get-user',
      data: {
        userId: this.data.userId
      }
    }).then(res => {
      // console.log('res66 ==>', res);
      this.setData({
        userInfo: res.result.data[0]
      })
      console.log(this.data.userInfo);
    }).catch(err => {
      console.log('err66 ==> ', err);
    })
  },
  determine(e) {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
    let index = this.data.userInfo.received.findIndex((value) => value._id == e.currentTarget.dataset.item._id);
    this.setData({
      ['userInfo.received[' + index + '].isSure']: true
    })
    
    wx.cloud.callFunction({
      name: 'save-received',
      data: {
        received: this.data.userInfo.received,
        userId: this.data.userId,
      },
    }).then(res => {
      wx.hideLoading();
      console.log('res ==> ', res);
      wx.showToast({
        title: res.result.msg,
        icon: 'none',
        duration: 2000
      })
    }).catch(err => {
      wx.hideLoading();
      console.log('err ==> ', err);
    })
  },
  onLoad(options) {

  },
  onShow() {
    this.getLoginUser()
  },
})