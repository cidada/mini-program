Page({
  data: {
    userInfo: {
      phone: '',
      password: ''
    }
  },
  //修改userInfo的数据
  updateUserInfo(e) {
    let key = e.currentTarget.dataset.key;
    this.data.userInfo[key] = e.detail.value;
  },
  //登录
  goLogin() {
    if (this.data.userInfo.phone != '' && this.data.userInfo.password != '') {
      //校验this.data.userInfo的数据合法性
      wx.showLoading({
        title: '加载中...',
        mask: true
      })
      //调用【login】云函数
      wx.cloud.callFunction({
        name: 'login',
        data: {
          ...this.data.userInfo
        }
      }).then(res => {
        wx.hideLoading();
        console.log('res ==> ', res);
        wx.showToast({
          title: res.result.msg,
          icon: 'none',
          duration: 2000
        })
        if (res.result.code === 1) {
          //将token保存在本地存储
          wx.setStorageSync('token', res.result.token);
          setTimeout(() => {
            this.goHome()
          },3000)
        }
      }).catch(err => {
        wx.hideLoading();
        console.log('err ==> ', err);
      })
    } else if (this.data.userInfo.phone == '' || this.data.userInfo.password == '') {
      wx.showToast({
        title: '手机号或密码不能为空',
        icon: 'none',
        duration: 2000
      })
    }
  },
  goHome() {
    wx.switchTab({
      url: '../home/home',
    })
  },
  onLoad(options) {

  },
  onShow() {

  }
})