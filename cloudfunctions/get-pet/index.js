// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
}) // 使用当前云环境

const db = cloud.database();
const _ = db.command;
// 云函数入口函数
exports.main = async (event, context) => {
  let currentUser = await db.collection('user-login').where({
    token: event.token
  }).get();
  let petData = null;
  if (currentUser.data.length === 1) {
    petData = await db.collection('pet').where({
      userId: _.neq(event.userId)
    }).skip(0).limit(5).orderBy('date', 'desc').get();
  } else {
    petData = await db.collection('pet').skip(0).limit(5).orderBy('date', 'desc').get();
  }
  return petData;
}