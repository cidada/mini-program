// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
}) // 使用当前云环境
const db = cloud.database();
// 云函数入口函数
exports.main = async (event, context) => {
  let date = new Date();
  return await db.collection('pet').add({
    data: {
      userId: event.userId,
      petAdopt: event.petAdopt,
      // imgFileIDs: event.imgFileIDs,
      userInfo:event.userInfo,
      date,
      time: date.getTime()
    }
  })
}