// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
}) // 使用当前云环境
const db = cloud.database();
const _ = db.command;
// 云函数入口函数
exports.main = async (event, context) => {
  if (event.location == '全国') {
    return await db.collection('pet').where(_.or([{
        userId: _.neq(event.userId),
        petAdopt: {
          petname: db.RegExp({
            regexp: '.*' + event.search,
            options: 'i',
          })
        }
      },
      {
        petAdopt: {
          breed: db.RegExp({
            regexp: '.*' + event.search,
            options: 'i',
          })
        }
        },
        {
        userInfo: {
          nickname: db.RegExp({
            regexp: '.*' + event.search,
            options: 'i',
          })
        },
      }
    ])).skip(0).limit(10).orderBy('date', 'desc').get();
  } else if(event.location != '全国'){
    return await db.collection('pet').where(_.or([{
      userId: _.neq(event.userId),
      userInfo: {
        address: event.location + '市'
      },
      petAdopt: {
        petname: db.RegExp({
          regexp: '.*' + event.search,
          options: 'i',
        })
      }
    }])).skip(0).limit(10).orderBy('date', 'desc').get();
  }


}